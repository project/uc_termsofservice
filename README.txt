You currently have the 'master' branch of this project checked out. There is
no code in the 'master' branch.

To obtain a working version of this module you must checkout one of the named
branches, such as 6.x-1.x or 7.x-1.x.  Please refer to the 'Version control'
tab (http://drupal.org/project/uc_termsofservice/git-instructions) on the
project page for details.
